# Mozu Shoppable Image Widget Documentation
######  🍺 🍺 🍺 🍺 Dipak Chandran P 🍺 🍺 🍺 🍺 🍺
---

>Requirement:  Create widget, so that one admin user can configure different points on a image, and associate products to those points. In the web page, these points should be visible to end user, which allow them to click on the points (hotspots) to show detailed view of the product associated to the point.


### Widget work flow architecture:
----
1. Using mozu admin site javascript variable in the app, Taco.core.util.UploadManager, upload an image from client system to Mozu File Manager.
2. Use the uploaded image url to show a preview of the image in the widget, using custom html elements.
3. Add click and drop listener for the preview element.
4. When admin user clicks on the preview element using the javascript click event calculate the position on the image and dynamically create hotspot(span) element and place it in the particular position.
5. While creating a hotspot element, make it draggable. Also provide a click handler for the hotspot element.
6. By making an hotspot element draggable, admin can easily reposition them on the preview. When admin drop the hotspot element on the preview, calculate position using javascript drop event, and update the position of the element.
7. When admin user click on the hotspot, will show a Ext.js window. This window will be containing a product select option, save option and delete option.
8. Admin can choose a product from product select option and click on save to associate the particular product to that hotspot.
9. Admin can click on the delete button to remove the particular hotspot.
10. Each time a change occurs in the preview, all data will be added/updated in a taco-array field, which is hidden.
###### Data Structure to store the shoppable image information:
```
{
	url 		: URL of the image uploaded into the mozu file manager
	width		: Actual width of the image
	height	: Actual height of the image
	preview_h	: Height of the image when showing on preview element
	preview_w	: Width of  the image when showing on preview element
	hotspots	: Array od hotspot information
	[
		{
			id 	: A unique id for the hotspot, which we can use for the manipulation
			x 	: Distance of the hotspot from the image left in percentage
			y 	: Distance of the hotspot from the image top in percentage
			pcode	: Product code associated with the hotspot.
		},….
	]
}
```



###### Widget Global Variables:

		draggingHotspot	: Used to store the hotspot element, which is currently editing. When ever hotspot is dragging or clicked, this property will get updated.

    	max_width       	: Maximum width of the preview area, if the uploaded image size width is greater then this value will reduce it to max_width and proportional reduces the height. (700 is the current value)

    	max_height      	: Maximum height of the preview area, if the uploaded image size height is greater then this value will reduce it to max_height and proportional reduces the width. (380 is the current value)

    	preview_w       	: After the calculation of the preview element width, this value will updated. which will store in data also to show the preview while loading the widget.

    	preview_h		: After the calculation of the preview element height, this value will updated. which will store in data also to show the preview while loading the widget.

###### Widget items details:

There are mainly four items in the widget.
>```tacofilefield``` : Used select file from local system, and upload to mozu file manager.
>
>```progressbar```  : Used to show the progress status of file uploading.
>
>```panel``` : Used to show the preview of shoppable image with the hotspots on it. The html property of this element is used to show the related details. The default html will contain a div with id ‘shopableimage-container’, preview container.
>
>```taco-arrayField``` : used to store data,  This field can store data, which we can access from storefront using its name : data.
>
>```taco-arrayField``` : Used to store the products codes, which are associated with the hotspots.


###### Widget work flow & functionality details:

###### STEP 1:
>Admin user drag and drop the widget into a drop zone. This will trigger the default listeners : afterrender function in the widget, this function will call updatePreview function. (STEP 2) else will be in STEP 9,
###### STEP 2:
>In updatePreview function, first will check if there is data in widget form x-type taco-array field element with name arraydata. If there is data,
Set the container width and height to the data preview width and hight value.
Create an image element with src as data url.
Clear all the items inside the container.
Append the image to container.
Call addHotspotsPreview function with data. (STEP 3)
Call bindClickDropEventListener function with image element (STEP 4)
###### STEP 3:
>In addHotspotsPreview function will loop through each of the items in hotpots property in the data. For each item will create and span element with left and top property as the current x,y value and pcode information as an attribute.
While creating each hotspot will associate click event to it and make it draggable true.
On clicking of hotspot will update global variable dragginHotspot and call function editHotspot() (STEP 8)
###### STEP 4:
>In bindClickDropEventListener will associate two events for the img element.
click : On clicking on the image it will call function  onContainerClick (STEP 5)
drop : On dropping  a hotspot element in to the image, will call function onContainerDrop (STEP 5)
###### STEP 5:
>Function will calculate the position, and calls calculate_and_place_element_postion function with x,y value. Also if its click a new element will created with null for id, x, y and code property. Will set it as the draggingHotspot global variable. (STEP 6)
###### STEP 6:
>calculate_and_place_element_postion will calculate the percentage position value for the hotspot, with respect to preview width and height, and update the element position. Then this function will call addOrUpdateHotspotProperty function with hotspot id, left percentage value, top percentage value and associated product code. (STEP 7)
###### STEP 7:
>addOrUpdateHotspotProperty If the id value is not null, this function will get the existing data and loop through the hotspots property and update the other informations of it, such as x, y and p-code. If the id value is null, it will create new object and append to the data, hotspots list.  Each time we update the hotspot it will used to set the form data field, and list of product codes associated to each hotspot will set in to the other form field, called pcodes.
###### STEP 8:
>In editHotspot function will show a Ext JS window element. Will call function getHotspotEditingWindow with action function, product code, id as parameter.
The Window contains, four element. Three buttons for different action, such as SAVE, DELETE, CANCLE. On clicking of these buttons will call action function, with a string parameter.
The other component on the window is taco-productfield, which is mozu custom element, used to list the products. From this user can select the product code want to associate, and click on save.

>For SAVE click, action function will check if there is product selected in taco-productfield, using its id, if elected it will call addOrUpdateHotspotProperty function with the elected product code and draggingHotspot id, x, y values. (STEP 7)

>For DELETE click, it will call removeHotSpot function, which will remove the current item information and element from preview. And update the data and p-codes  field.
###### STEP 9:
>This is stage when we are dropping the widget into the drop zone for the first time.
>
>Here admin can upload an image, using taco file field. when change is happening on this field it will call the onUploadFile function, this function will call onComplete, when upload is finishes, with the data information of image url, width and height.

>In onCompete function will create a sample data structure and update the data array field, and calls updatePreview function (STEP 2)



### Theme Architecture and functionality details:
----

In theme level we have following files :
- Templates :
	1. templates/Widgets/misc/shopableimageEditor.hypr
	2. templates/modules/product/products-shoppable.hypr.live
	3. templates/modules/product/product-shoppable.hypr.live
- Scripts :
	1. scripts/widgets/shopableimageEditor.js
- Stylesheets :
	2. stylesheets/widgets/shopableimageEditor.less
###### File Details
>templates/Widgets/misc/shopableimageEditor.hypr : This is the display template of our widget will have the structure of shoppable image and hotspots.
>
>templates/modules/product/products-shoppable.hypr.live : This is used to preload the data associated of all the product model. We will preload this with the widget id property to make it unique.
>
>templates/modules/product/product-shoppable.hypr.live : Will have the structure of pop with product details, which will be shown when we click on the hotspot.

###### JavaScript functionality flow:

In javascript there are global variables which should have the same size of the popup window.
* MODAL_POPUP_WIDTH 	= 320  width of modal popup
* MODAL_POPUP_HEIGHT 	= 150  height of the modal popup
* HOTSPOT_WIDTH		= 20  width of hotspot
* HOTSPOT_HEIGHT		= 20 height of the hotspot

* pmodel  Used to store the Backbone.Mozu.ProductModel, of the currently active hotspot.

In javascript we will be listening to following events:

###### Clicking of hotspot:
> when we click on a hotspot, will get the current product code, data-id value from the clicked element attribute. data-id value will be unique for the widget after dropping them in to the container. so we can use this information to uniquely identify the elements even if we drop more than once the same widget. 
> Will hide all the popup curet showing 
> Remove active class from all the hotspots. 
> Add active class to the current hotspot. 
> If there is  product code available, will fetch correspond element which used to show the preview of hotspot. 
> Calculate the position of the hotspot with respect to the current hotspot position. All hotspot left and top value are in percentage, all calculation logic below will be done in percentage only.
```
if hotspot.left > 50:
	modal.popup.left = hotspot.left - hotspot.width - modalpopup.width
else:
	modal.popup.left = hotspot.left + hotspot.width
if hotspot.top > 50:
	modal.popup.top = hotspot.top - hotspot.height - modalpopup.height
else:
	modal.popup.left = hotspot.height + hotspot.heigth
```
>Update modal position and make it style display properly to block. 
>Will call function createProductModel(data_id,pcode) to set the pmodel value. This function will do the following
calls require.mozuData with data_id to fetch all the product data associated with the current shoppable image.
Loop through the items to fetch the correct product item.
Create a new product model using Mozu Backbone product model
return it. 
>Finally to the new product model will associate event listener, for added to cart event. So that we can update cart monitor when item is get added to cart.

###### Clicking on close button on the hotspot popup
>This will fetch the current shoppable image data id, and code of the product from the clicked element and hide the popup.
Update the style class for hotspot.

###### Clicking on add-to-cart on the hotspot popup
>In this function will call the ProductModel addtocart function using the global product model variable available in the javascript function context.

-----
* NOTE 1:
>All the calculations are done in percentage vice, and the position in percentage is calculated from widget level itself.

* NOTE 2:
>When ever a product view is showing via the popup, we have its complete product model, with out making any api calls. We can use this model to implement further product level features, like changing options and updating price.

* NOTE 3:
>Whole this documentation is with respect to the development flow.
